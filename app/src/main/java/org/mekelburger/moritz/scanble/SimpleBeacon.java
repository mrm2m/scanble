package org.mekelburger.moritz.scanble;

import java.util.Arrays;

/**
 * Created by moritz on 10.11.2014.
 */
public class SimpleBeacon {
    private float[] position;
    private float distance;
    private float distanceError;
    private float rssi;

    public SimpleBeacon(float[] position, float distance, float distanceError, float rssi) {
        this.position = position;
        this.distance = distance;
        this.distanceError = distanceError;
        this.rssi = rssi;
    }

    public float[] getPosition() {
        return position;
    }

    public float getDistance() {
        return distance;
    }

    public float getDistanceError() {
        return distanceError;
    }

    @Override
    public String toString() {
        return String.format("SimpleBeacon: %s | %.2f | %.2f | %.2f", Arrays.toString(position),
                this.getDistance(), this.getDistanceError(), this.rssi);
    }
}
